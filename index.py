from flask import Flask, render_template
from flask_dropzone import Dropzone



app = Flask(__name__) 
dropzone = Dropzone(app)


@app.route('/upload', methods=['GET', 'POST'])
def upload():

    if request.method == 'POST':
        f = request.files.get('file')
        f.save(os.path.join('the/path/to/save', f.filename))

    return 'upload template'

@app.route('/')
def home():
    return render_template('home.html')

if __name__ == '__main__':
    app.run(debug=True);
